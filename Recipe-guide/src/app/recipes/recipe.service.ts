import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class RecipeService {
  //replace "EventEmitter" with "Subject"
   //recipeSelected = new EventEmitter<Recipe>();
   //recipeSelected = new Subject<Recipe>();
   recipesChanged = new Subject<Recipe[]>();

  // private receipes: Recipe[] = [
  //   new Recipe('Yummy tummy Crab curry','What else do u need?','https://thekaravaliwok.com/wp-content/uploads/2017/07/2017-07-17-16-16-55.jpg',[
  //     new Ingredient('Coke',10),
  //     new Ingredient('French Fries',10),
  //     new Ingredient('Stuffed burger',20),
  //   ]),
  //   new Recipe('Creamy Mushroom Soup','Delicious Soup with mild spices - juz Awesome','https://www.tasteofhome.com/wp-content/uploads/2018/01/Quick-Cream-of-Mushroom-Soup_EXPS_SDAM17_11767_B12_02_8b-1.jpg',[
  //     new Ingredient('boneless chicken',20),
  //     new Ingredient('Garlic Bread',30),
  //     new Ingredient('Magrita Pizza',30),
  //   ])
  // ];

  private receipes: Recipe[] = [];

  constructor(private sltService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]){
    this.receipes = recipes;
    this.recipesChanged.next(this.receipes.slice());
  }

  getRecipes(){
    return this.receipes.slice();
  }

  getRecipe(index: number){
      return this.receipes[index];
  }

  addIngredientsToShoppingList(ingredient: Ingredient[]){
    this.sltService.addNewIngredients(ingredient);
  }

  addRecipe(recipe: Recipe) {
    this.receipes.push(recipe);
    this.recipesChanged.next(this.receipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.receipes[index] = newRecipe;
    this.recipesChanged.next(this.receipes.slice());
  }

  deleteRecipe(index: number) {
    this.receipes.splice(index, 1);
    this.recipesChanged.next(this.receipes.slice());
  }

}

import { Component, OnInit,OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from './../recipe.model';
import { RecipeService } from './../recipe.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit,OnDestroy {
  // @Output()  recipeWasSelected = new EventEmitter<Recipe>();
  // receipes: Recipe[] = [
  //   new Recipe('A Test Recipe','This is simply a test','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/vegetarian-recipes-1582652898.jpg?crop=1.00xw:1.00xh;0,0&resize=640:*'),
  //   new Recipe('A New Recipe','This is simply a test','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/vegetarian-recipes-1582652898.jpg?crop=1.00xw:1.00xh;0,0&resize=640:*')
  // ];

  receipes: Recipe[];
  subscription: Subscription;

  constructor(private recipeService: RecipeService,
              private router: Router,
              private route: ActivatedRoute) { 

  }

  ngOnInit(): void {
    this.subscription = this.recipeService.recipesChanged
      .subscribe(
        (receipes: Recipe[]) => {
          this.receipes = receipes;
        }
      );
      this.receipes = this.recipeService.getRecipes();
  }

  // onRecipeSelected(recipe: Recipe){
  //   this.recipeWasSelected.emit(recipe);
  // }

  onNewRecipe(){
      this.router.navigate(['new'],{relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
